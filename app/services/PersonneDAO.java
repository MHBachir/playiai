package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonneDAO extends JDBC {

	public String ajouter(Personne c) throws ClassNotFoundException, SQLException {

		try {
			prepareStat = this.jdbc().prepareStatement("insert into  java.personne values (default, ?, ?, ?) ");
			prepareStat.setString(1, c.getName());
			prepareStat.setString(2, c.getPrenom());
			prepareStat.setInt(3, c.getAge());
			// prepareStat.setDate(4, new java.sql.Date(2009, 12, 11));
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}

	}

	public List<Personne> listes() throws ClassNotFoundException, SQLException {

		statment = this.jdbc().createStatement();
		List<Personne> listes = new ArrayList<Personne>();
		Personne c = new Personne();
		// les resultat de la requete sont stocker dans l'objet result
		result = statment.executeQuery("select * from java.personne");

		while (result.next()) {

			c.setName(result.getString("nom"));
			c.setPrenom(result.getString("prenom"));
			c.setAge(Integer.valueOf(result.getString("age")));
			listes.add(c);
			// on viende l'objet personne avant de repasser la boucle
			c = new Personne();

		}

		return listes;

	}

	private String suppression(int idp) throws SQLException {
		// result = statment.executeQuery("select * from java.comments where
		// myuser =?"+name);
		prepareStat = con.prepareStatement("select * from java.personne where id =?; ");
		prepareStat.setInt(1, id);
		result = prepareStat.executeQuery();

		if (result.next()) {
			System.out.println("l'element au nom :" + result.getString("nom") + " sera supprimer");
			prepareStat = con.prepareStatement("delete from java.personne where id= ? ; ");
			prepareStat.setInt(1, id);
			prepareStat.executeUpdate();
			return "OK";
		} else {
			System.out.println("Aucun Element correspondant");
			return "NO";
		}
	}

}
