package controllers;

import play.data.FormFactory;
import play.mvc.*;
import play.mvc.Http.Request;

/**
 * This controller contains an action to handle HTTP requests to the
 * application's home page.
 */
public class HomeController extends Controller {

	FormFactory formFactory;

	/**
	 * An action that renders an HTML page with a welcome message. The
	 * configuration in the <code>routes</code> file means that this method will
	 * be called when the application receives a <code>GET</code> request with a
	 * path of <code>/</code>.
	 */
	public Result index() {
		String test = "ceci est un test de retour";
		int nbr = 2555;
		return ok(views.html.index.render(test,nbr));
	}

	public Result test(Request request) {
//		String viewMode = formFactory.form().bindFromRequest(request).get("myuser");
//		String var = "je marche";
//		return ok(views.html.test.render(var, request));
		return TODO(request);
	}
	
	public Result exercice() {
		String viewMode = "test: 12";
		int n = 10;
		Long d = 15L;
		
		return ok(views.html.exercice.render(viewMode,n,d ));
	}
}
