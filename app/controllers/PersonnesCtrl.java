package controllers;

import java.sql.SQLException;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import services.Personne;
import services.PersonneDAO;
import play.mvc.Result;

public class PersonnesCtrl extends Controller {

	FormFactory dataForm;
	PersonneDAO personneDAO;

	@Inject
	public PersonnesCtrl(FormFactory dataForm, PersonneDAO personneDAO) {
		super();
		this.dataForm = dataForm;
		this.personneDAO = personneDAO;
	}

	public Result ajoutPersonne(Request request) throws ClassNotFoundException, SQLException {

		Form<Personne> form = dataForm.form(Personne.class).bindFromRequest(request);
		Personne personne = form.get();
		
		if(personneDAO.ajouter(personne).equals("ok"))
			System.out.println("ajout effectuer avec success");
		else
			System.err.println("ajout non effectuer" + personneDAO.ajouter(personne));
		
		
		int n1 =0;//Integer.valueOf(dataForm.form().bindFromRequest(request).get("nbr1"));
		int n2 = 0;//Integer.valueOf(dataForm.form().bindFromRequest(request).get("nbr2"));
		
		//personneDAO.listes();
		
		return ok(views.html.test.render(personne,personneDAO.listes(),n1,n2));

	}

	

	public Result afficherFormulaire(Request request) {
		return ok(views.html.personne.render(request));
	}

}
